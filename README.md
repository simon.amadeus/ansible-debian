# 📚 Ansible Playbook Documentation

---

## 🔍 **Overview**
This Ansible playbook provides configurations for setting up, upgrading, and deploying applications across different hosts. It's designed to be modular, making it easy to manage, execute, and maintain.

---

## 📂 **Directory Structure**

### 🛠 **Playbooks**:
1. **Setup**: Initial configuration for hosts.
2. **Upgrade**: Implement upgrades on hosts.
3. **Deploy**: Roll out specific applications to targeted hosts.

### 🎭 **Roles**:
- **`common`**: Base configurations for all hosts.
- **`docker`**: Specific configurations for Docker.
- **`nginx`**: Configurations tailored for Nginx.
- **`common-upgrade`**: General upgrades for all hosts.
- **`docker-upgrade`**: Upgrades specific to Docker.
- **`nginx-upgrade`**: Upgrades specific to Nginx.
- **`deploy-user`**: Configurations related to the deployment user.
- **`docker-applications`**: Deployment configurations for Docker-based applications.

---

## 🏷 **Using Tags**

In Ansible, tags provide the flexibility to execute only specific parts of a playbook, making playbooks more modular and efficient. Our playbook uses several tags to differentiate between setup, upgrade, and deployment processes.

### **Benefits**:
1. **Granular Control**: Execute only the tasks that have the specified tags, saving time.
2. **Modularity**: Break down large playbooks into smaller chunks, making it easier to understand and maintain.
3. **Efficiency**: Re-run only specific tasks as needed without executing the entire playbook.


### **Setup Tags**:
- **`setup`**: Represents all tasks related to the initial setup and configurations.
  - **`common`**: Tasks associated with setting up common tools.
  - **`docker`**: Tasks related to Docker configurations.
  - **`nginx`**: Tasks for setting up Nginx.

### **Upgrade Tags**:
- **`upgrade`**: Represents all upgrade-related tasks.
  - **`common-upgrade`**: General upgrade tasks.
  - **`docker-upgrade`**: Docker specific upgrade tasks.
  - **`nginx-upgrade`**: Nginx specific upgrade tasks.

### **Deployment Tags**:
- **`deploy`**: Represents all deployment-related tasks.
  - **`deploy-user`**: Deploying user configurations.
  - **`deploy-applications`**: Deploying specific applications.

---

## 📌 **Inventory Structure**
Our inventories are organized by environment. Each environment has its own inventory file, which contains the host definitions for that environment.
> **Example**: `development.yml`.

## 📝 **Variables**
Variables are used to define specific values that can be referenced throughout the playbook. Variables are defined in the group_vars directory, which contains a file for each group of hosts.

**Example**:
- `group_vars/all.yml`
- `group_vars/docker_hosts.yml`

---

## 🚀 **How to Run**

Below are the commands to execute specific sections of the playbook:

```bash
# 🌐 Setup for all hosts
ansible-playbook -i development.yml site.yml --tags "common" -e "ansible_user=root" --ask-vault-pass

# 🐳 Setup specifically for Docker hosts
ansible-playbook -i development.yml site.yml --tags "setup" --limit "docker_hosts" --ask-vault-pass

# ⏫ Upgrade all hosts
ansible-playbook -i development.yml site.yml --tags "upgrade" --ask-vault-pass

# ⏫ Upgrade nginx only on docker hosts
ansible-playbook -i development.yml site.yml --tags "nginx-upgrade" --limit "docker_hosts" --ask-vault-pass

# 📦 Deploy applications
ansible-playbook -i development.yml site.yml --tags "deploy" --ask-vault-pass

# Secrets encryption
# remember to use the --ask-vault-pass flag
```

---

<!-- add docs on secret encryption for ./group_vars/<group>/secrets.yml -->
## 🔒 **Secrets Encryption**

Secrets are encrypted using Ansible Vault. The encrypted files are stored in the `group_vars/<group>/secrets.yml` file. To manage the secrets files, use the following commands:

```bash
ansible-vault edit group_vars/<group>/secrets.yml # edit the file

ansible-vault encrypt group_vars/<group>/secrets.yml # encrypt the file

ansible-vault decrypt group_vars/<group>/secrets.yml # decrypt the file
```

<!-- add note on importance of ansible version for encryption/decryption compatibility -->
> **Note**: If problems arise when decrypting the secrets file, ensure that the Ansible version is compatible with the version used to encrypt the file. Version: `2.15.4`.





---

⚠️ **Note**:

- Always operate from the playbook's root directory when running the `ansible-playbook` command.
- Regularly verify the inventory file (`development.yml`) to confirm correct host definitions.
- Consistently review and test the roles for continued relevance and accurate functionality.
